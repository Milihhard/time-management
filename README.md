![Timee](res/icon.svg)

# Timee: Time management software #

Timee is a software that helps people managing their time.

## How to use it?

With tasks and subtaks, define all you want to do during a determined time. You can attribute weight to know how many hours you need for a subtask. Graphics help visualise the weight of each tasks and subtasks.

![Time management](res/time-management.png)

The burndown chart show you if you are in time or not by checking subtasks you have done.

![Burndown chart](res/burndown-chart.png)


## developement

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test


# lint all JS/Vue component files in `src/`
npm run lint

```

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[45a3e22](https://github.com/SimulatedGREG/electron-vue/tree/45a3e224e7bb8fc71909021ccfdcfec0f461f634) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
