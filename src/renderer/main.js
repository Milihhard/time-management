import Vue from 'vue';
import axios from 'axios';

import App from './App';
import router from './router';
import store from './store';
import { MdTabs, MdSubheader, MdMenu, MdEmptyState, MdToolbar, MdList, MdDrawer, MdApp, MdCard, MdTable, MdDatepicker, MdButton, MdContent, MdRipple, MdDialog, MdField, MdIcon, MdSpeedDial, MdTooltip } from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

import i18n from './i18n';

Vue.use(MdCard);
Vue.use(MdTable);
Vue.use(MdDatepicker);
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdRipple);
Vue.use(MdDialog);
Vue.use(MdField);
Vue.use(MdIcon);
Vue.use(MdSpeedDial);
Vue.use(MdTooltip);
Vue.use(MdApp);
Vue.use(MdDrawer);
Vue.use(MdToolbar);
Vue.use(MdList);
Vue.use(MdEmptyState);
Vue.use(MdMenu);
Vue.use(MdSubheader);
Vue.use(MdTabs);

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  i18n,
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app');
