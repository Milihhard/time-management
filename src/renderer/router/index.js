import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'time-management',
      component: require('@/views/TimeManagement').default
    },
    {
      path: '/burndown-chart',
      name: 'burndown-chart',
      component: require('@/views/BurndownChartView').default
    },
    {
      path: '/planning-board',
      name: 'planning-board',
      component: require('@/views/PlanningBoardView').default
    },
    {
      path: '/planning-board-result',
      name: 'planning-board-result',
      component: require('@/views/PlanningBoardResultView').default
    },
    {
      path: '/help',
      name: 'help',
      component: require('@/views/Help').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});
