export default class Color {
  constructor(r, g, b) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  toString() {
    return `rgb(${this.r},${this.g},${this.b})`;
  }

  static getColorsRange(start, end, n) {
    let colors = [];
    for (let i = 0; i < n; i++) {
      colors.push(getColor(start, end, n, i));
    }
    return colors;
  }
}
function getColor(colorStart, colorEnd, n, i) {
  return new Color(
    getColorPart(colorStart, colorEnd, n, i, 'r'),
    getColorPart(colorStart, colorEnd, n, i, 'g'),
    getColorPart(colorStart, colorEnd, n, i, 'b')
  );
}
function getColorPart(colorStart, colorEnd, n, i, part) {
  return Math.round(colorStart[part] +
        Math.pow(-1, colorStart[part] < colorEnd[part] ? 2 : 1) *
        (Math.abs(colorStart[part] - colorEnd[part]) / (n === 1 ? 1 : n - 1) * i));
}
